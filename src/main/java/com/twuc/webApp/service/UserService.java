package com.twuc.webApp.service;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.domain.UserRepository;

import java.util.Optional;

public class UserService {
    private UserRepository repository;

    public Optional<GetUserResponse> getUser(Long id) {
        return repository.findById(id).map(GetUserResponse::new);
    }
}
